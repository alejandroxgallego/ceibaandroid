package co.ceibaandroid.data.local

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import co.ceibaandroid.data.entities.Posts
import co.ceibaandroid.data.entities.Users

@Dao
interface UserDao {

    @Query("SELECT * FROM users_table")
    fun getAllUsers() : LiveData<List<Users>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAllUsers(users: List<Users>)

    @Query("SELECT * FROM post_table WHERE userId = :id")
    fun getAllPostUser(id: Int): LiveData<List<Posts>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllPosts(posts: List<Posts>)
}