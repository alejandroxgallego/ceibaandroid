package co.ceibaandroid.data.remote

import javax.inject.Inject

class UsersRemoteDataSource @Inject constructor(
    private val usersService: UsersService
): BaseDataSource() {

    suspend fun getUsers() = getResult { usersService.getAllUsers() }
    suspend fun getPost(id: Int) = getResult { usersService.getAllPost(id) }
}