package co.ceibaandroid.data.remote

import co.ceibaandroid.data.entities.Posts
import co.ceibaandroid.data.entities.Users
import co.ceibaandroid.utils.Endpoints.GET_POST
import co.ceibaandroid.utils.Endpoints.GET_USERS
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface UsersService {

    @GET( GET_USERS)
    suspend fun getAllUsers() : Response<ArrayList<Users>>

    @GET(GET_POST)
    suspend fun getAllPost(@Query("userId={id}") id: Int): Response<ArrayList<Posts>>

}