package co.ceibaandroid.data.repository

import co.ceibaandroid.data.local.UserDao
import co.ceibaandroid.data.remote.UsersRemoteDataSource
import co.ceibaandroid.utils.DataAccessStrategy
import javax.inject.Inject

class UsersRepository @Inject constructor(
    private val remoteDataSource: UsersRemoteDataSource,
    private val localDataSource: UserDao,
    private val dataAccessStrategy: DataAccessStrategy
) {
    fun getAllPost(id: Int) = dataAccessStrategy.performGetOperation(
        databaseQuery = { localDataSource.getAllPostUser(id) },
        networkCall = { remoteDataSource.getPost(id) },
        saveCallResult = { localDataSource.insertAllPosts(it) }
    )

    fun getUsers() = dataAccessStrategy.performGetOperation(
        databaseQuery = { localDataSource.getAllUsers() },
        networkCall = { remoteDataSource.getUsers() },
        saveCallResult = { localDataSource.insertAllUsers(it) }
    )
}