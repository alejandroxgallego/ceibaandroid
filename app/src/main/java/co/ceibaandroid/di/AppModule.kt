package co.ceibaandroid.di

import android.content.Context
import co.ceibaandroid.data.local.AppDatabase
import co.ceibaandroid.data.local.UserDao
import co.ceibaandroid.data.remote.UsersRemoteDataSource
import co.ceibaandroid.data.remote.UsersService
import co.ceibaandroid.data.repository.UsersRepository
import co.ceibaandroid.utils.DataAccessStrategy
import co.ceibaandroid.utils.Endpoints.URL_BASE
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideRetrofit(gson: Gson) : Retrofit = Retrofit.Builder()
        .baseUrl( URL_BASE)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()

    @Provides
    fun provideGson(): Gson = GsonBuilder().create()

    @Provides
    fun provideUserService(retrofit: Retrofit): UsersService = retrofit.create(UsersService::class.java)

    @Singleton
    @Provides
    fun provideUserRemoteDataSource(userService: UsersService) = UsersRemoteDataSource(userService)

    @Singleton
    @Provides
    fun provideDatabase(@ApplicationContext appContext: Context) = AppDatabase.getDatabase(appContext)

    @Singleton
    @Provides
    fun provideUserDao(db: AppDatabase) = db.userDao()

    @Singleton
    @Provides
    fun provideRepository(remoteDataSource: UsersRemoteDataSource,
                          localDataSource: UserDao,
                          dataAccessStrategy: DataAccessStrategy
    ) = UsersRepository(remoteDataSource, localDataSource, dataAccessStrategy)

}
