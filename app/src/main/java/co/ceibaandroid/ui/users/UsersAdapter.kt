package co.ceibaandroid.ui.users

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import co.ceibaandroid.data.entities.Users
import co.ceibaandroid.databinding.ItemUserBinding

class UsersAdapter (private val listener: UserItemListener) : RecyclerView.Adapter<UsersAdapterViewHolder>() {

    interface UserItemListener {
        fun onClickedUser(userId: Int)
    }

    private val items = ArrayList<Users>()

    fun setData(newList: ArrayList<Users>) {
        val newValues = SearchUtil(items, newList)
        val result = DiffUtil.calculateDiff(newValues)
        items.clear()
        items.addAll(newList)
        result.dispatchUpdatesTo(this)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UsersAdapterViewHolder {
        val binding: ItemUserBinding = ItemUserBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return UsersAdapterViewHolder(binding, listener)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holderAdapter: UsersAdapterViewHolder, position: Int) = holderAdapter.bind(items[position])

    inner class SearchUtil(private val oldList: ArrayList<Users>, private val newList: ArrayList<Users>) : DiffUtil.Callback() {

        override fun getOldListSize(): Int {
            return oldList.size
        }

        override fun getNewListSize(): Int {
            return newList.size
        }

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition].id == newList[newItemPosition].id
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition] == newList[newItemPosition]
        }
    }
}
