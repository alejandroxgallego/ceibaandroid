package co.ceibaandroid.ui.users

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import co.ceibaandroid.data.entities.Users
import co.ceibaandroid.databinding.ItemUserBinding

class UsersAdapterViewHolder(private val itemBinding: ItemUserBinding, private val listener: UsersAdapter.UserItemListener) : RecyclerView.ViewHolder(itemBinding.root),
    View.OnClickListener {

    private lateinit var users: Users

    init {
        itemBinding.btnViewPost.setOnClickListener(this)
    }

    fun bind(item: Users) {
        this.users = item
        itemBinding.name.text = item.name
        itemBinding.email.text = item.email
        itemBinding.phone.text = item.phone
    }

    override fun onClick(v: View?) {
        listener.onClickedUser(users.id)
    }
}