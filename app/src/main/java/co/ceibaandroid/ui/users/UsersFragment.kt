package co.ceibaandroid.ui.users

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import co.ceibaandroid.R
import co.ceibaandroid.data.entities.Users
import co.ceibaandroid.databinding.UsersFragmentBinding
import co.ceibaandroid.utils.Resource
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class UsersFragment : Fragment(), UsersAdapter.UserItemListener {

    private var _binding: UsersFragmentBinding? = null
    private val binding get() = _binding!!

    private val viewModel: UsersViewModel by viewModels()
    private lateinit var adapter: UsersAdapter
    private var allUsers = ArrayList<Users>()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = UsersFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
        setupObservers()
    }

    private fun setupRecyclerView() {
        adapter = UsersAdapter(this)
        binding.rvUsers.layoutManager = LinearLayoutManager(requireContext())
        binding.rvUsers.adapter = adapter
    }


    private fun setupObservers() {
        binding.etSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s.toString().trim().isNotEmpty()) {
                    val filter = allUsers.filter { it.name.contains(s.toString().trim(), true) }
                    if (filter.isNullOrEmpty()) {
                        binding.txtMessage.visibility = View.VISIBLE
                    } else {
                        binding.txtMessage.visibility = View.GONE
                    }
                     adapter.setData(filter as ArrayList<Users>)
                } else {
                    binding.txtMessage.visibility = View.GONE
                    adapter.setData(allUsers)
                }
            }
            override fun afterTextChanged(s: Editable?) {}
        })



        viewModel.users.observe(viewLifecycleOwner) { users ->
            when (users.status) {
                Resource.Status.SUCCESS -> {
                    binding.progressBar.visibility = View.GONE
                    if (!users.data.isNullOrEmpty()) {
                        allUsers.clear()
                        allUsers.addAll(users.data)
                        adapter.setData(ArrayList(users.data))
                    }
                }
                Resource.Status.ERROR -> {
                    Toast.makeText(activity, users.message, Toast.LENGTH_SHORT).show()
                }
                Resource.Status.LOADING ->{
                    binding.progressBar.visibility = View.VISIBLE
                }
            }
        }
    }

    override fun onClickedUser(id: Int) {
        val position = id-1
        findNavController().navigate(
            R.id.action_teamsFragment_to_detailFragment,
            bundleOf(
                "id" to id,
                "email" to allUsers[position].email,
                "phone" to allUsers[position].phone,
                "name" to allUsers[position].name
            )
        )
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


}
