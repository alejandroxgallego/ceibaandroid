package co.ceibaandroid.ui.users

import androidx.lifecycle.ViewModel
import co.ceibaandroid.data.repository.UsersRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class UsersViewModel @Inject constructor(
    repository: UsersRepository
) : ViewModel() {
    val users = repository.getUsers()
}
