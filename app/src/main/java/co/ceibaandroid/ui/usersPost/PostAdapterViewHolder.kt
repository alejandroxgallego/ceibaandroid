package co.ceibaandroid.ui.usersPost

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import co.ceibaandroid.data.entities.Posts
import co.ceibaandroid.databinding.ItemPostBinding

class PostAdapterViewHolder (private val itemBinding: ItemPostBinding) : RecyclerView.ViewHolder(itemBinding.root),
    View.OnClickListener {

    private lateinit var post: Posts

    fun bind(item: Posts) {
        this.post = item
        itemBinding.title.text = item.title
        itemBinding.body.text = item.body
    }

    override fun onClick(p0: View?) {}

}