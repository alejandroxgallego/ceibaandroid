package co.ceibaandroid.ui.usersPost

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import co.ceibaandroid.data.entities.Posts
import co.ceibaandroid.databinding.ItemPostBinding

class PostsAdapter : RecyclerView.Adapter<PostAdapterViewHolder>() {

    private val items = ArrayList<Posts>()

    fun setItems(items: ArrayList<Posts>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostAdapterViewHolder {
        val binding: ItemPostBinding = ItemPostBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return PostAdapterViewHolder(binding)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holderAdapter: PostAdapterViewHolder, position: Int) = holderAdapter.bind(items[position])

}



