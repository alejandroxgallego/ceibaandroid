package co.ceibaandroid.ui.usersPost

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import co.ceibaandroid.databinding.PostsFragmentBinding
import co.ceibaandroid.utils.Resource
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PostsFragment : Fragment() {

    private var _binding: PostsFragmentBinding? = null
    private val binding get() = _binding!!
    private lateinit var adapter: PostsAdapter
    private val viewModel: PostsViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = PostsFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
        setupRecyclerView()
        setupObservers()
    }

    private fun setupView() {
        arguments?.getInt("id")?.let { viewModel.start(it) }
        arguments?.getString("phone")?.let { binding.userPhone.text = it }
        arguments?.getString("name")?.let { binding.nameUser.text = it }
        arguments?.getString("email")?.let { binding.userEmail.text = it }
    }

    private fun setupRecyclerView() {
        adapter = PostsAdapter()
        binding.rvPosts.layoutManager = LinearLayoutManager(requireContext())
        binding.rvPosts.adapter = adapter
    }

    private fun setupObservers() {
        viewModel.posts.observe(viewLifecycleOwner) { items ->
            when (items.status) {
                Resource.Status.SUCCESS -> {
                    binding.progressBar.visibility = View.GONE
                    binding.teamDetailCl.visibility = View.VISIBLE
                    items.data?.let { ArrayList(it) }?.let { adapter.setItems(it) }
                }
                Resource.Status.ERROR -> {
                    Toast.makeText(activity, items.message, Toast.LENGTH_SHORT).show()
                }
                Resource.Status.LOADING -> {
                    binding.progressBar.visibility = View.VISIBLE
                    binding.teamDetailCl.visibility = View.GONE
                }
            }
        }
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}
