package co.ceibaandroid.ui.usersPost

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.switchMap
import co.ceibaandroid.data.entities.Posts
import co.ceibaandroid.data.repository.UsersRepository
import co.ceibaandroid.utils.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class PostsViewModel @Inject constructor(private val repository: UsersRepository) : ViewModel() {

    private val _id = MutableLiveData<Int>()

    private val _post = _id.switchMap { id ->
        repository.getAllPost(id)
    }
    val posts: LiveData<Resource<List<Posts>>> = _post

    fun start(id: Int) {
        _id.value = id
    }

}
