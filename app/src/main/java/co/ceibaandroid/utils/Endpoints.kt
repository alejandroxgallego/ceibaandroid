package co.ceibaandroid.utils

object Endpoints {
    const val URL_BASE = "https://jsonplaceholder.typicode.com/"
    const val GET_USERS = "users"
    const val GET_POST = "posts"
}